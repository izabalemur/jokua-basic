extends KinematicBody2D
const BALA = preload("res://bala.tscn")
export (int) var speed = 200
const GRABITATEA = 10
export (int) var salto= -300
var norantza = 1
var velocity = Vector2()
var egoera = "geldi"

func get_input():
    velocity.x = 0
    if Input.is_action_pressed('Right'):
        velocity.x = speed
        $AnimatedSprite.flip_h = false
        norantza = 1
    if Input.is_action_pressed('Left'):
        velocity.x = -speed
        $AnimatedSprite.flip_h = true
        norantza = -1
    if Input.is_action_pressed('Up') and is_on_floor():
        velocity.y = salto
        var egoera = 'salto'
    if Input.is_action_just_pressed('Fire'):
        print("fire")
        var bala = BALA.instance()
        get_parent().add_child(bala)
        bala.set_norantza(norantza)
        bala.position = $Position2D.global_position
        
func _physics_process(delta):
    get_input()
    velocity.y += GRABITATEA
    if velocity.x != 0:
        $AnimatedSprite.play("ibili")
    else:
        $AnimatedSprite.play("geldi")

    move_and_slide(velocity,Vector2(0, -1))

   