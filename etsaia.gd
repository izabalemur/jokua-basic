extends KinematicBody2D

export (int) var speed = 200
const GRABITATEA = 10

var norantza = 1
var velocity = Vector2()
var egoera = 'default'

func _physics_process(delta):
    velocity.y += GRABITATEA
    velocity.x = speed * norantza* delta
    $AnimatedSprite.play(egoera)
    move_and_slide(velocity)

    if is_on_wall():
        norantza = norantza * -1

    if norantza == 1:
        $AnimatedSprite.flip_h = false
    else:
        $AnimatedSprite.flip_h = true